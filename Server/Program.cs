﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public static class SocketListener
    {
        public static void StartListening()
        {
            // Establish the local endpoint for the socket.  
            // Dns.GetHostName returns the name of the
            // host running the application.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.  
            Socket listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and
            // listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);
                // Start listening for connections.  
                Console.WriteLine("Waiting for a connection...");
                // Program is suspended while waiting for an incoming connection.  
                while (ThreadPool.QueueUserWorkItem(Client, listener.Accept())) ;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        private static void Client(object o)
        {
            using (Socket handler = (Socket)o)
            {
                try
                {
                    Console.WriteLine($"Socket connected to {handler.RemoteEndPoint}");
                    // Data buffer for incoming data.  
                    byte[] bytes = new Byte[1024];

                    const string endComunication = "<EOF>";
                    string data;
                    // An incoming connection needs to be processed.  
                    do
                    {
                        // Incoming data from the client.  
                        int bytesRec = handler.Receive(bytes);
                        data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        // Show the data on the console.  

                        //WebUtility.UrlDecode(data); //The converted string is expected to conform to the UTF-8 format.
                        Console.WriteLine($"{handler.RemoteEndPoint}: {data}");

                        // Echo the data back to the client.  
                        //byte[] msg = Encoding.ASCII.GetBytes(data);
                        //handler.Send(msg);
                    }
                    while (data.IndexOf(endComunication) <= -1);

                    handler.Shutdown(SocketShutdown.Both);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.WriteLine("Close connection");
        }



        public static int Main(String[] args)
        {
            StartListening();
            return 0;
        }
    }
}
